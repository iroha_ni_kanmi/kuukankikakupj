﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fenderrio.ImageWarp;

public class MappingAppController : MonoBehaviour {
	
	//Imageを動かすためのImageObj
	public Transform TopLeftImage;
	public Transform BottomLeftImage;
	public Transform TopRightImage;
	public Transform BottomRightImage;
	
	//RawImageWarp
	public GameObject mappingObj;
	private RawImageWarp mappingAppImageWarp;
	
	//EditMode用関数
	public GameObject editText;
	public string editKey;
	private bool flgEdit;
	
	//初期値
	private Vector3 startTL, startBL, startTR, startBR;
	private float startWidth, startHeight;
	
	// Use this for initialization
	void Start () {
		
		
		flgEdit = false;
		
		mappingAppImageWarp = mappingObj.GetComponent<RawImageWarp>();
		LoadData(mappingAppImageWarp, mappingAppImageWarp.name);
		
		//サイズの初期値取得
		var sizeMappingObj = mappingObj.GetComponent<RectTransform>();		
		startWidth  = sizeMappingObj.GetWidth();
		startHeight = sizeMappingObj.GetHeight();
		
		//offsetの初期値
		startTL = mappingAppImageWarp.cornerOffsetTL;
		startBL = mappingAppImageWarp.cornerOffsetBL;
		startTR = mappingAppImageWarp.cornerOffsetTR;
		startBR = mappingAppImageWarp.cornerOffsetBR;
		
	}
	
	void Update () {
		
		if(flgEdit){
			//EditModeのテキスト表示
			editText.SetActive(flgEdit);
			
			//常にImageのコーナーとを同期
			SynchroOffsets(mappingAppImageWarp);

			//拡大・縮小処理
			if(Input.GetKey(KeyCode.UpArrow)){
				changeScale(1f);
			}
			if(Input.GetKey(KeyCode.DownArrow)){
				changeScale(-1f);
			}
					
			//リセット処理
			if (Input.GetKey(KeyCode.Escape)) {
				ResetValues(mappingObj, mappingAppImageWarp);
			}
		}
		
		else{
			//EditModeのテキスト非表示
			editText.SetActive(flgEdit);
		}
		
		//EditMode：On/Off 切り替え
		if(Input.GetKeyDown(editKey)){
			flgEdit = !flgEdit;
		}
		SaveData(mappingAppImageWarp, mappingAppImageWarp.name);
	}
	
	void SynchroOffsets(RawImageWarp mappingAppImageWarp){
		//常にImageのコーナーとを同期
		mappingAppImageWarp.cornerOffsetTL = TopLeftImage.localPosition;
		mappingAppImageWarp.cornerOffsetBL = BottomLeftImage.localPosition;
		mappingAppImageWarp.cornerOffsetTR = TopRightImage.localPosition;
		mappingAppImageWarp.cornerOffsetBR = BottomRightImage.localPosition;
		
		SaveData(mappingAppImageWarp, mappingAppImageWarp.name);
	}
	
	void changeScale(float axisValue){

		//拡大・縮小処理
		var sizeMappingObj = mappingObj.GetComponent<RectTransform>();		
		var width  = sizeMappingObj.GetWidth() + axisValue;
		var height = sizeMappingObj.GetHeight() + axisValue;
		
		sizeMappingObj.SetSize( width, height );
		
	}
	
	void ResetValues(GameObject mappingObj, RawImageWarp mappingAppImageWarp){
		//サイズの初期化
		var sizeMappingObj = mappingObj.GetComponent<RectTransform>();	
		sizeMappingObj.SetSize( startWidth, startHeight );
		
		//Offsetの初期化
		mappingAppImageWarp.cornerOffsetTL = startTL;
		mappingAppImageWarp.cornerOffsetBL = startBL;
		mappingAppImageWarp.cornerOffsetTR = startTR;
		mappingAppImageWarp.cornerOffsetBR = startBR;
		
		TopLeftImage.localPosition = startTL;
		BottomLeftImage.localPosition = startBL;
		TopRightImage.localPosition = startTR;
		BottomRightImage.localPosition = startBR;

		SaveData(mappingAppImageWarp, mappingAppImageWarp.name);

	}
	
	
	public void SaveData(RawImageWarp mappingAppImageWarp, string KeyPref){
		//TopLeftImageの保存
		PlayerPrefs.SetInt(KeyPref, 1); 
		
		PlayerPrefs.SetFloat(KeyPref + "TopLeftImageX",mappingAppImageWarp.cornerOffsetTL.x);
		PlayerPrefs.SetFloat(KeyPref + "TopLeftImageY",mappingAppImageWarp.cornerOffsetTL.y);
		PlayerPrefs.SetFloat(KeyPref + "TopLeftImageZ",mappingAppImageWarp.cornerOffsetTL.z);
		
		PlayerPrefs.SetFloat(KeyPref + "BottomLeftImageX",mappingAppImageWarp.cornerOffsetBL.x);
		PlayerPrefs.SetFloat(KeyPref + "BottomLeftImageY",mappingAppImageWarp.cornerOffsetBL.y);
		PlayerPrefs.SetFloat(KeyPref + "BottomLeftImageZ",mappingAppImageWarp.cornerOffsetBL.z);
		
		PlayerPrefs.SetFloat(KeyPref + "TopRightImageX",mappingAppImageWarp.cornerOffsetTR.x);
		PlayerPrefs.SetFloat(KeyPref + "TopRightImageY",mappingAppImageWarp.cornerOffsetTR.y);
		PlayerPrefs.SetFloat(KeyPref + "TopRightImageZ",mappingAppImageWarp.cornerOffsetTR.z);

		PlayerPrefs.SetFloat(KeyPref + "BottomRightImageX",mappingAppImageWarp.cornerOffsetBR.x);
		PlayerPrefs.SetFloat(KeyPref + "BottomRightImageY",mappingAppImageWarp.cornerOffsetBR.y);
		PlayerPrefs.SetFloat(KeyPref + "BottomRightImageZ",mappingAppImageWarp.cornerOffsetBR.z);

		PlayerPrefs.Save ();
	}
	
	public void LoadData(RawImageWarp mappingAppImageWarp, string KeyPref){
		
		//RawImageWarpの初期値を取得する
		if(PlayerPrefs.HasKey(KeyPref)){
			mappingAppImageWarp.cornerOffsetTL = new Vector3(PlayerPrefs.GetFloat(KeyPref + "TopLeftImageX"),PlayerPrefs.GetFloat(KeyPref + "TopLeftImageY"),PlayerPrefs.GetFloat(KeyPref + "TopLeftImageZ"));
			
			mappingAppImageWarp.cornerOffsetBL = new Vector3(PlayerPrefs.GetFloat(KeyPref + "BottomLeftImageX"),PlayerPrefs.GetFloat(KeyPref + "BottomLeftImageY"),PlayerPrefs.GetFloat(KeyPref + "BottomLeftImageZ"));
			
			mappingAppImageWarp.cornerOffsetTR = new Vector3(PlayerPrefs.GetFloat(KeyPref + "TopRightImageX"),PlayerPrefs.GetFloat(KeyPref + "TopRightImageY"),PlayerPrefs.GetFloat(KeyPref + "TopRightImageZ"));
			
			mappingAppImageWarp.cornerOffsetBR = new Vector3(PlayerPrefs.GetFloat(KeyPref + "BottomRightImageX"),PlayerPrefs.GetFloat(KeyPref + "BottomRightImageY"),PlayerPrefs.GetFloat(KeyPref + "BottomRightImage"));
		}
	}
}
