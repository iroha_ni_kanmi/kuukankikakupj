﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MappingEditAssistController : MonoBehaviour
{
    //編集中、他のビューが表示されないようにする
    [Header("BackViewSetting"), SerializeField]
    public GameObject BackView;
    public string keyBack;

    [Header("FrontViewSetting"), SerializeField]
    public GameObject FrontView;
    public string keyFront;

    private bool flgBack,flgFront = false;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(keyBack)){
            if(!flgFront){
                FrontView.SetActive(!FrontView.activeSelf);
                flgBack = !flgBack;
            }
        }

        if(Input.GetKeyDown(keyFront)){
            if(!flgBack){
                BackView.SetActive(!BackView.activeSelf);
                flgFront = !flgFront;
            }
        }

    }
}
