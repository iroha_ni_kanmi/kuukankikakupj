﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using MidiJack;

public partial class KuukanKikakuApp : MonoBehaviour
{
    //動画の再生モードを定義
    public enum MOVIE_MODE{
        Default,
        Section,
    }
    private MOVIE_MODE movie_mode;

    //動画再生を完了する値のセット(フロントの映像基準)
    private float endTime;

    // Start is called before the first frame update
    void Start()
    {
        //デフォルト映像セッティング
        SetDefaultVideoClip();

        //endTimeをUniRxで監視
		playerFront.ObserveEveryValueChanged( _ => _.time)
			.Subscribe (mode => {
                switch(movie_mode){
                    case MOVIE_MODE.Default:
                        //MOVIE_MODE.Default ... ループ再生を行う
                        //Debug.Log("通った02");
                        break;

                    case MOVIE_MODE.Section:
                        //MOVIE_MODE.Sectionの時 ... MOVIE_MODE.Defaultに戻し、ループ映像に戻す

                        //Debug.Log("現在：" + playerFront.time.ToString() + "、終了：" + endTime.ToString() + "CountTime：" + CountPushTimes.ToString());
                        //endTimeであれば、デフォルトの動画にする

                        //※float型でみてしまうと、Updateの処理とendTimeリセット周りの処理がうまくいかないので、
                        // int型に変換し、比較する
                        if((int)playerFront.time == (int)endTime){
                            SetDefaultVideoClip();
                        }
                    break;
                }
		    });
        
        
        
    }

    // Update is called once per frame
    void Update()
    {

        //===============動画切り替え===============
        //keyが押されたかどうかの判定
		if(Input.GetKeyDown("1")){
            SwitchVideoMode();
        }
    }

    void NoteOn(MidiChannel channel, int note, float velocity){
        //キーが入力された時の処理
        SwitchVideoMode();
    }

    void NoteOff(MidiChannel channel, int note)
    {
       
		Debug.Log("NoteOff: " + note);
    }

    void OnEnable()
    {
        MidiMaster.noteOnDelegate += NoteOn;
        MidiMaster.noteOffDelegate += NoteOff;
        
    }

    void OnDisable()
    {
        MidiMaster.noteOnDelegate -= NoteOn;
        MidiMaster.noteOffDelegate -= NoteOff;
        
    }

    void SwitchVideoMode(){

        //===============動画切り替え===============
        switch(movie_mode){
            //初回のムービーが流れている場合
            case MOVIE_MODE.Default:
                //キー操作対応の動画をセットする
                OnClickButtonAtDefaultMode();

                movie_mode = MOVIE_MODE.Section;
                break;

            //特定ムービーを流している最中の場合
            case MOVIE_MODE.Section:
                //キー操作対応の、次の動画をセットする
                OnClickButtonAtSection();

                break;
        }
    }

}
