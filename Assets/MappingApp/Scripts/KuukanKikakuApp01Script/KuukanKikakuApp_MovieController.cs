﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public partial class KuukanKikakuApp{
    //動画再生関係の処理をここに記載していく

    //動画を描画するVideoPlayer定義
    [Header("MovieSetting"), SerializeField]
    public VideoPlayer playerFront;
    public VideoPlayer playerBack;

    [Header("DefaultMovieSetting"), SerializeField]
    public VideoClip DefaultMovie_front;
    public VideoClip DefaultMovie_back;

    [Header("SectionMovieSetting"), SerializeField]
    public VideoClip SectionMovie_front;
    public VideoClip SectionMovie_back;

    public void SetDefaultVideoClip(){
        //デフォルトの動画をセット
        playerFront.clip = DefaultMovie_front;
        playerBack.clip = DefaultMovie_back;

        playerFront.isLooping = true;
        playerBack.isLooping = true;

        //動画再生準備
        playerFront.Prepare();
        playerBack.Prepare();

        //動画終了時間とモードをセット
        endTime = (float)playerFront.clip.length;
        movie_mode = MOVIE_MODE.Default;
        
        //動画を再生
        playerFront.Play();;
        playerBack.Play();;
        
        
    }

    public void SetSectionVideoClip(){
        //セクションの動画をセット
        playerFront.clip = SectionMovie_front;
        playerBack.clip = SectionMovie_back;

        playerFront.isLooping = false;
        playerBack.isLooping = false;

        //動画再生準備
        playerFront.Prepare();
        playerBack.Prepare();

        //動画を再生
        playerFront.Play();
        playerBack.Play();
    }

    public void SetVideoFrame(float time){
        
        //動画の再生位置をセット
        playerFront.time = time;
        playerBack.time = time;

        //動画再生準備
        playerFront.Prepare();
        playerBack.Prepare();

        //動画を再生
        playerFront.Play();
        playerBack.Play();
    }
    

}
