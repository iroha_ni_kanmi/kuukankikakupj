﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class KuukanKikakuApp02{
    //キー操作関係の処理をここに記載していく

    //キーが押された回数
    private int CountPushTimes;

    public List<float> sectionTime;
    

    public void OnClickButtonAtDefaultMode(){
        //デフォルトモードでキーが押された場合の処理
        CountPushTimes = 0;

        //section動画をセット
        SetSectionVideoClip();
        SetVideoFrame(sectionTime[CountPushTimes]);

        //endTimeをセット
        endTime = sectionTime[CountPushTimes+1];
    }

    public void OnClickButtonAtSection(){
        
        //キータップモード（セクションモード）でキーが押された場合の処理
        CountPushTimes += 1;

        //動画をセット
        SetVideoFrame(sectionTime[CountPushTimes]);

        //CountPushTImesを元に、終了時間を算出
        //プレイ対象の映像が最後のものなら、動画最後のフレームを格納する
        if((CountPushTimes + 1) >= sectionTime.Count){
            //TODO:フレーム最後の秒数をendTimeに格納する
            endTime = (float)playerFront.clip.length;
            CountPushTimes = -1;

        }else{
            endTime = sectionTime[CountPushTimes + 1];
        }
    }
}
